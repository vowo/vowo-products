package products

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

// Repository provides access the products store.
type Repository interface {
	FindAll(ctx context.Context) []*Product
	GetByID(ctx context.Context, id uuid.UUID) (*Product, error)
}

// Product domain model
type Product struct {
	Name                 string                `json:"name"`
	ID                   uuid.UUID             `json:"id"`
	PricePerKg           float32               `json:"pricePerKg"`
	ImageURL             string                `json:"imageUrl"`
	Currency             string                `json:"currency"`
	Ingredients          string                `json:"ingredients"`
	Description          string                `json:"description"`
	NutritionInformation *NutritionInformation `json:"nutritionInformation"`
}

// NutritionInformation domain model
type NutritionInformation struct {
	EnergyKcal           int32 `json:"energyKcal"`
	EnergyKj             int32 `json:"energyKj"`
	FatG                 int32 `json:"fatG"`
	SaturatedFattyAcidsG int32 `json:"saturatedFattyAcidsG"`
	CarbohydrateG        int32 `json:"carbohydrateG"`
	OfWhichSugarG        int32 `json:"ofWhichSugarG"`
	DietaryFiberG        int32 `json:"dietaryFiberG"`
	ProteinG             int32 `json:"proteinG"`
	SaltG                int32 `json:"saltG"`
}
