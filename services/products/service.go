package products

import (
	"context"

	opentracing "github.com/opentracing/opentracing-go"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// Service is the interface to access products.
type Service interface {
	GetAllProducts(ctx context.Context) ([]*Product, error)
	GetProductByID(ctx context.Context, id uuid.UUID) (*Product, error)
}

type service struct {
	tracer             opentracing.Tracer
	logger             log.Factory
	productsRepository Repository
}

func (s *service) GetAllProducts(ctx context.Context) ([]*Product, error) {
	products := s.productsRepository.FindAll(ctx)
	return products, nil
}

func (s *service) GetProductByID(ctx context.Context, id uuid.UUID) (*Product, error) {
	return s.productsRepository.GetByID(ctx, id)
}

// NewService creates an products service with necessary dependencies.
func NewService(tracer opentracing.Tracer, logger log.Factory, productsRepository Repository) Service {
	return &service{
		logger:             logger,
		tracer:             tracer,
		productsRepository: productsRepository,
	}
}
