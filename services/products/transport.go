package products

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/vowo/vowo-corelib/pkg/httperr"
	"gitlab.com/vowo/vowo-corelib/pkg/httputils"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
)

// MakeHandler returns a handler for the products service.
func (s *Server) MakeHandler(endpoints *Set) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(s.EncodeError),
	}

	allProductsHandler := kithttp.NewServer(
		endpoints.allProducts,
		decodeHTTPProductsRequest,
		httputils.EncodeHTTPGenericResponse,
		append(opts, kithttp.ServerBefore(tracing.HTTPToContext(s.tracer, "products", s.logger)))...,
	)

	getByIDHandler := kithttp.NewServer(
		endpoints.getByID,
		decodeGetByIDRequest,
		httputils.EncodeHTTPGenericResponse,
		append(opts, kithttp.ServerBefore(tracing.HTTPToContext(s.tracer, "products", s.logger)))...,
	)

	r := mux.NewRouter()
	r.Handle("/", allProductsHandler).Methods("GET")
	r.Handle("/{productID}", getByIDHandler).Methods("GET")
	r.NotFoundHandler = http.HandlerFunc(httperr.HandleNotFound)
	r.MethodNotAllowedHandler = http.HandlerFunc(httperr.HandleMethodNotAllowed)

	return r
}

func decodeHTTPProductsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return nil, nil
}

func decodeGetByIDRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id, ok := mux.Vars(r)["productID"]
	if !ok || id == "" {
		return nil, errInvalidArgument
	}

	productUUID, err := uuid.FromString(id)
	if err != nil {
		return nil, errors.Wrap(errInvalidArgument, err.Error())
	}

	return getByIDRequest{id: productUUID}, nil
}

func encodeGetByIDRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getByIDRequest)
	r.URL.Path = "/" + req.id.String()
	return nil
}

func encodeHTTPProductsRequest(ctx context.Context, r *http.Request, request interface{}) error {
	return nil
}

func decodeHTTPProductsResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}

	var resp allProductsResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

func decodeGetByIDResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}

	var resp getByIDResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

// EncodeError encode errors from business-logic
func (s *Server) EncodeError(ctx context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	case errInvalidArgument:
		s.logger.For(ctx).Info("bad request", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
	default:
		s.logger.For(ctx).Error("server error", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
