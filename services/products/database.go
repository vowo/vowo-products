package products

import (
	"context"
	"fmt"
	"sync"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

type productsRepository struct {
	mtx      sync.RWMutex
	products map[uuid.UUID]*Product
	tracer   opentracing.Tracer
	logger   log.Factory
}

func (r *productsRepository) FindAll(ctx context.Context) []*Product {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	allProducts := make([]*Product, 0, len(r.products))
	for _, product := range r.products {
		allProducts = append(allProducts, product)
	}

	return allProducts
}

func (r *productsRepository) GetByID(ctx context.Context, id uuid.UUID) (*Product, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	if val, ok := r.products[id]; ok {
		return val, nil
	}

	return nil, errors.Wrap(errNotFound, fmt.Sprintf("product with id %v", id))
}

// newProductsRepository returns a new instance of a in-memory products repository.
func newProductsRepository(tracer opentracing.Tracer, logger log.Factory) *productsRepository {
	productID1, _ := uuid.FromString("a2226b8a-8100-4c44-8796-e86cd4175b0a")
	productID2, _ := uuid.FromString("cc669ef5-0d4d-4730-8f57-5ab7518b63f4")
	productID3, _ := uuid.FromString("292aa14b-0fc2-43ce-bcd6-595a44442b00")

	nutrients := &NutritionInformation{
		EnergyKcal:           100,
		EnergyKj:             85,
		FatG:                 23,
		SaturatedFattyAcidsG: 10,
		CarbohydrateG:        2,
		OfWhichSugarG:        0,
		DietaryFiberG:        0,
		ProteinG:             75,
		SaltG:                0,
	}
	return &productsRepository{
		tracer: tracer,
		logger: logger,
		products: map[uuid.UUID]*Product{
			productID1: {
				Name:                 "Gantrisch Salami",
				Description:          `Die Tiere werden von Mamishaus nach Thun in die Metzgerei gebracht gebracht (Transportdauer 30 Minuten). Nach drei Tagen umröten wird die Salami in den Küchenrauch nach Lohnstorf transportiert mit unserem Lieferwägeli 3.5t. Dort verbleiben sie ca 3-4 Wochen zum Trocknen. Anschliessend werden sie wieder zurück nach Riggi geholt, verpackt und zum Verkauf bereitgestellt.`,
				Ingredients:          "Rindfleisch 20%, Schweinefleisch 60%, Rückenspeck 20%",
				NutritionInformation: nutrients,
				ID:                   productID1,
				PricePerKg:           25.00,
				ImageURL:             "https://ffglqa.by.files.1drv.com/y4mXY3C2wfFJGi3QdQSXXko5-vxJI_mQAMa7C_hK3YB6wGd0zI9s7sc5lF0zr9bQfbYZUsMkrssm_fe9rwn76V5nf8eEnSiOHVeVF6nOOaFw_2IA5fGFUiFPCrnILzt_xUoKz7miuU1ffMoFpx7LvuSfoYgfZFsB4-yHx1akslLs5dCLwpwvfzDEhvNW2TbjS205bhTKDg2KW-lSRp5UnZw-A?width=3024&height=4032&cropmode=none",
				Currency:             "CHF",
			},
			productID2: {
				Name:                 "Suppenknochen",
				Description:          `Die Tiere werden von uns in die Dorfmetzgerei von Heinz Sommer in Sumiswald gebracht (Transportdauer 15 Minuten). Dort verarbeiten und vakuumieren wir das Fleisch.`,
				Ingredients:          "100% Rindsknochen",
				NutritionInformation: nutrients,
				ID:                   productID2,
				PricePerKg:           15.00,
				ImageURL:             "https://image.migros.ch/2017-large/ce8a2bc2ee1f0d4febbc24707be4f4ed03132e89/suppenknochen.jpg",
				Currency:             "CHF",
			},
			productID3: {
				Name:                 "Optigal Pouletschnitzel 2 Stück",
				Description:          `Die Tiere werden von uns in die Dorfmetzgerei von Heinz Sommer in Sumiswald gebracht (Transportdauer 15 Minuten). Dort verarbeiten und vakuumieren wir das Fleisch.`,
				Ingredients:          "100% Huhn",
				NutritionInformation: nutrients,
				ID:                   productID3,
				PricePerKg:           28.00,
				ImageURL:             "https://image.migros.ch/2017-large/09b94ff31d7071d4cbc1cdfd82dee85f562541eb/optigal-pouletschnitzel-2-stueck.jpg",
				Currency:             "CHF",
			},
		},
	}
}
