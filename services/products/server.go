package products

import (
	"net/http"

	"github.com/heptiolabs/healthcheck"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-lib/metrics"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
)

// Server implements Products service
type Server struct {
	hostPort           string
	healthHostPort     string
	tracer             opentracing.Tracer
	logger             log.Factory
	productsRepository Repository
}

// NewServer creates a new Products server
func NewServer(hostPort string, healthHostPort string, tracer opentracing.Tracer, metricsFactory metrics.Factory, logger log.Factory, jAgentHostPort string) *Server {
	repo := newProductsRepository(
		tracing.Init("mysql", metricsFactory.Namespace("mysql", nil), logger, jAgentHostPort),
		logger.With(zap.String("component", "mysql")),
	)

	return &Server{
		hostPort:           hostPort,
		healthHostPort:     healthHostPort,
		tracer:             tracer,
		logger:             logger,
		productsRepository: repo,
	}
}

// Run starts the Products server
func (s *Server) Run() error {
	var productsService Service
	productsService = NewService(s.tracer, s.logger, s.productsRepository)
	endpoint := NewEndpoint(productsService, s.tracer, s.logger)
	mux := s.MakeHandler(endpoint)

	s.logger.Bg().Info("Starting", zap.String("address", "http://"+s.hostPort))
	return http.ListenAndServe(s.hostPort, mux)
}

// RunHealthAndReadinessProbes starts health and readiness probes
func (s *Server) RunHealthAndReadinessProbes() {
	health := healthcheck.NewHandler()

	//TODO implement proper checks
	// Our app is not happy if we've got more than 100 goroutines running.
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))
	s.logger.Bg().Info("Starting Healthcheck", zap.String("address", "http://"+s.healthHostPort))
	err := http.ListenAndServe(s.healthHostPort, health)
	if err != nil {
		s.logger.Bg().Error("could not start readiness probe", zap.Error(err))
	}
}
