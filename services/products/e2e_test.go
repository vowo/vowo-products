package products

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
	jexpvar "github.com/uber/jaeger-lib/metrics/expvar"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
	httpexpect "gopkg.in/gavv/httpexpect.v1"
)

type ProductsTestSuite struct {
	suite.Suite
	handler http.Handler
}

//TODO create schema from swagger spec
var schema = `{
	"type": "object",
	"properties": {
		"products": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"Name": {
          				"type": "string"
      				},
      				"ID": {
          				"type": "string"
      				}
				}
			}
		}
	}	
}`

func (suite *ProductsTestSuite) SetupSuite() {
	metricsFactory := jexpvar.NewFactory(10)
	zapLogger, _ := zap.NewDevelopment()
	zapLogger = zapLogger.With(zap.String("service", "gateway"))
	logger := log.NewFactory(zapLogger)
	tracer := tracing.Init("products", metricsFactory.Namespace("products", nil), logger, "0.0.0.0:6831")
	repo := newProductsRepository(
		tracer,
		logger.With(zap.String("component", "mysql")),
	)
	s := &Server{logger: logger, tracer: tracer, productsRepository: repo}
	productsService := NewService(s.tracer, s.logger, s.productsRepository)
	endpoint := NewEndpoint(productsService, s.tracer, s.logger)
	suite.handler = s.MakeHandler(endpoint)
}

func (suite *ProductsTestSuite) TestGetAllProducts() {
	// arrange
	server := httptest.NewServer(suite.handler)
	defer server.Close()
	e := httpexpect.New(suite.T(), server.URL)

	// act & assert
	products := e.GET("/").
		Expect().
		Status(http.StatusOK).JSON()

	// validate JSON schema
	products.Schema(schema)
}

func (suite *ProductsTestSuite) TestUnknownMethod() {
	// arrange
	server := httptest.NewServer(suite.handler)
	defer server.Close()
	e := httpexpect.New(suite.T(), server.URL)

	// act & assert
	e.POST("/").
		Expect().
		Status(http.StatusMethodNotAllowed).JSON().Object().ContainsKey("error")
}

func (suite *ProductsTestSuite) TearDownSuite() {
	//TODO teardown
}

func TestProductsTestSuiteTestSuite(t *testing.T) {
	suite.Run(t, new(ProductsTestSuite))
}
