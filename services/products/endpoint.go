package products

import (
	"context"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	opentracing "github.com/opentracing/opentracing-go"
	uuid "github.com/satori/go.uuid"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// allProductsRequest for an http request
type allProductsRequest struct {
}

type getByIDRequest struct {
	id uuid.UUID
}

type getByIDResponse struct {
	Product *Product `json:"product,omitempty"`
	Err     error    `json:"-"` // should be intercepted by Failed/errorEncoder
}

// allProductsResponse to an http request
type allProductsResponse struct {
	Products []*Product `json:"products,omitempty"`
	Err      error      `json:"-"` // should be intercepted by Failed/errorEncoder
}

// Set collects all of the endpoints that compose an products service.
type Set struct {
	allProducts endpoint.Endpoint
	getByID     endpoint.Endpoint
}

// GetAllProducts implements the products.Service
func (s *Set) GetAllProducts(ctx context.Context) ([]*Product, error) {
	resp, err := s.allProducts(ctx, allProductsRequest{})
	if err != nil {
		return nil, err
	}

	response := resp.(allProductsResponse)
	return response.Products, response.Err
}

// GetProductByID implements the products.Service
func (s *Set) GetProductByID(ctx context.Context, id uuid.UUID) (*Product, error) {
	resp, err := s.getByID(ctx, getByIDRequest{id})
	if err != nil {
		return nil, err
	}

	response := resp.(getByIDResponse)
	return response.Product, response.Err
}

// NewEndpoint creates a new products endpoint
func NewEndpoint(s Service, tracer opentracing.Tracer, logger log.Factory) *Set {
	var allProductsEndpoint endpoint.Endpoint
	{
		allProductsEndpoint = makeGetAllProductsEndpoint(s)
		allProductsEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(allProductsEndpoint)
		allProductsEndpoint = kitopentracing.TraceServer(tracer, "products")(allProductsEndpoint)
		allProductsEndpoint = LoggingMiddleware(logger)(allProductsEndpoint)
	}

	var getByIDEndpoint endpoint.Endpoint
	{
		getByIDEndpoint = makeGetByIDEndpoint(s)
		getByIDEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(getByIDEndpoint)
		getByIDEndpoint = kitopentracing.TraceServer(tracer, "products")(getByIDEndpoint)
		getByIDEndpoint = LoggingMiddleware(logger)(getByIDEndpoint)
	}

	return &Set{
		allProducts: allProductsEndpoint,
		getByID:     getByIDEndpoint,
	}
}

func makeGetAllProductsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		products, err := s.GetAllProducts(ctx)

		if err != nil {
			return nil, err
		}

		return &allProductsResponse{Products: products, Err: err}, nil
	}
}

func makeGetByIDEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getByIDRequest)
		product, err := s.GetProductByID(ctx, req.id)

		if err != nil {
			return nil, err
		}

		return &getByIDResponse{Product: product, Err: err}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = allProductsResponse{}
)

// Failed implements endpoint.Failer.
func (r allProductsResponse) Failed() error { return r.Err }
