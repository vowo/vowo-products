package products

import (
	"net/url"
	"time"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	httptransport "github.com/go-kit/kit/transport/http"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/httputils"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
)

// NewHTTPClient creates a new products.Service backed by an HTTP server living at the
// remote instance. Instance is expected to come from a service discovery system,
// so likely of the form "host:port".
func NewHTTPClient(instance string, tracer opentracing.Tracer, logger log.Factory) (Service, error) {
	instance = httputils.HTTPPrefixURL(instance)
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	options := []httptransport.ClientOption{}
	var allProductsEndpoint endpoint.Endpoint
	{
		allProductsEndpoint = httptransport.NewClient(
			"GET",
			httputils.AddPathToURL(u, "/"),
			encodeHTTPProductsRequest,
			decodeHTTPProductsResponse,
			append(options, httptransport.ClientBefore(tracing.ContextToHTTP(tracer, logger)))...,
		).Endpoint()
		allProductsEndpoint = kitopentracing.TraceClient(tracer, "products")(allProductsEndpoint)
		allProductsEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "products",
			Timeout: 30 * time.Second,
		}))(allProductsEndpoint)
	}

	var getByIDEndpoint endpoint.Endpoint
	{
		getByIDEndpoint = httptransport.NewClient(
			"GET",
			httputils.AddPathToURL(u, "/"),
			encodeGetByIDRequest,
			decodeGetByIDResponse,
			append(options, httptransport.ClientBefore(tracing.ContextToHTTP(tracer, logger)))...,
		).Endpoint()
		getByIDEndpoint = kitopentracing.TraceClient(tracer, "products")(getByIDEndpoint)
		getByIDEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "products",
			Timeout: 30 * time.Second,
		}))(getByIDEndpoint)
	}

	return &Set{
		allProducts: allProductsEndpoint,
		getByID:     getByIDEndpoint,
	}, nil
}
