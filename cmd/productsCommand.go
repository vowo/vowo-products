package cmd

import (
	"net"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"gitlab.com/vowo/vowo-products/services/products"
	"go.uber.org/zap"
)

// productsCommand represents the products command
var productsCommand = &cobra.Command{
	Use:   "products",
	Short: "Starts Products service",
	Long:  `Starts Products service.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		zapLogger := logger.With(zap.String("service", "products"))
		logger := log.NewFactory(zapLogger)
		server := products.NewServer(
			net.JoinHostPort(productsOptions.serverInterface, strconv.Itoa(productsOptions.serverPort)),
			net.JoinHostPort(productsOptions.serverInterface, strconv.Itoa(productsOptions.serverHealthPort)),
			tracing.Init("products", metricsFactory.Namespace("products", nil), logger, jAgentHostPort),
			metricsFactory,
			logger,
			jAgentHostPort,
		)

		go server.RunHealthAndReadinessProbes()
		return logError(zapLogger, server.Run())
	},
}

var (
	productsOptions struct {
		serverInterface  string
		serverPort       int
		serverHealthPort int
	}
)

func init() {
	RootCmd.AddCommand(productsCommand)

	//TODO read port from env
	productsCommand.Flags().StringVarP(&productsOptions.serverInterface, "bind", "", "0.0.0.0", "interface to which the Products server will bind")
	productsCommand.Flags().IntVarP(&productsOptions.serverPort, "port", "p", 8082, "port on which the Products server will listen")
	productsCommand.Flags().IntVarP(&productsOptions.serverHealthPort, "healthPort", "e", 8182, "port on which the health check of the products service will listen")
}
